<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://hsnet.pl
 * @since             2.0.0
 * @package           Jadwiga-Gallery
 *
 * @wordpress-plugin
 * Plugin Name:       Google+ Gallery
 * Plugin URI:        https://gitlab.com/jakub-wojtowicz/jdpp-gallery-plugin
 * Description:       Display gallery on school's website forked from WPPicasa
 * Version:           1.1.3
 * Author:            HSNet.pl
 * Author URI:        https://hsnet.pl/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       jadwiga-gallery
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cws-google-picasa-pro-activator.php
 */
function activate_cws_google_picasa_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cws-google-picasa-pro-activator.php';
	CWS_Google_Picasa_Pro_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cws-google-picasa-pro-deactivator.php
 */
function deactivate_cws_google_picasa_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cws-google-picasa-pro-deactivator.php';
	CWS_Google_Picasa_Pro_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cws_google_picasa_pro' );
register_deactivation_hook( __FILE__, 'deactivate_cws_google_picasa_pro' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cws-google-picasa-pro.php';

require plugin_dir_path( __FILE__ ) . 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakub-wojtowicz/jdpp-gallery-plugin/',
	__FILE__,
	'jdpp-gallery-plugin'
);
$myUpdateChecker->setBranch('master');

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    2.0.0
 */
function run_jadwiga_gallery() {

    $plugin = new CWS_Google_Picasa_Pro();
	$plugin->run();

}
run_jadwiga_gallery();
