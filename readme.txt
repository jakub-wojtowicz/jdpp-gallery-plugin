=== Jadwiga Google Photos Plugin ===

Contributors: wojtokuba
Tags: jadwiga-gallery
Requires at least: 3.0.1
Tested up to: 5.1.1
Stable tag: 1.0.0

Google Photos Jadwiga plugin forked from https://wordpress.org/plugins/google-picasa-albums-viewer/


== Description ==
Fork of the gallery from https://wordpress.org/plugins/google-picasa-albums-viewer/

== Prerequisites ==

1. PHP5.4 or above
2. cURL running on your web host


== Installation ==

1. Unzip into your `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Make your settings, Admin->Settings->Google Picasa
4. Use the 'Display Album' shortcode [cws_gpp_albums_gphotos access=own] on a page of your choice.
5. To display the album's images place the shortcode, [cws_gpp_images_in_album_gphotos] on a page
6. Update the shortcode used in step 4 to include the result_page option. [cws_gpp_albums_gphotos access=own results_page='page-slug-here']


== Frequently Asked Questions ==

= What if I don't want to display private (unlisted) albums? =

Simple select "Public" option from the 'Show which albums' dropdown menu.

= Does this work with Google Photos? =

Yes this plugin works with images and albums stored and created in Google Photos too.

= Can Users Download the Original Image File =

Yes this is an option on the <a href="http://www.cheshirewebsolutions.com/?utm_source=cws_gpp_config&utm_medium=text-link&utm_content=readme&utm_campaign=cws_gpp_plugin" rel="friend">Pro version</a>

= The Cache Doesn't Seem to be Working =

Log in to you web host and check the cache/ folder is writable by the web server.


== Screenshots ==

1. An example of Google Photos Album Grid View.
2. An example of Google Photos Album List View.
3. This is the default settings page.
4. This is an eaxmple of the Albums shortcode [cws_gpp_albums_gphotos access=own].
5. This is an example of the lightbox displaying photo you clicked on.


== Shortcodes ==

* [cws_gpp_albums_gphotos access=own] Will display your albums.

* [cws_gpp_images_in_album_gphotos] This is a place holder to display the results of an album that has been clicked on, on a separate page.

* [cws_gpp_images_by_albumid_gp id='6625618881344302193'] Will display images in the album specified via the 'id' attribute. You must use album id from Album ID helper Page


== Shortcode Usage ==

* Display Albums Covers in a Carousel
[cws_gpp_albums_gphotos access=own theme=carousel dots=1 slidestoshow=3 slidestoscroll=1 autoplay=1 arrows=1 results_page=results-carousel show_details=1 show_title=1 num_results=6 hide_albums='Profile Photos,Auto Backup']

* Display Album Covers in a Grid View
[cws_gpp_albums_gphotos access=own theme=grid results_page=results-grid show_title=1 show_details=1 num_results=6 hide_albums='Auto Backup,Profile Photos']

* Display Album Covers in a List View
[cws_gpp_albums_gphotos access=own theme=list results_page=results-list show_title=1 show_details=1 thumb_size=185 num_results=3]

* Display Images from Clicked Album Cover in a Carousel View
[cws_gpp_images_in_album_gphotos theme=carousel show_title=0 thumbsize=150]

* Display Images from Clicked Album Cover in a Grid View
[cws_gpp_images_in_album_gphotos theme=grid show_title=1 show_details=1]

* Display Images from Clicked Album Cover in a List View
[cws_gpp_images_in_album_gphotos theme=list show_title=1 show_details=1 num_results=13 thumb_size=250]

* Display Images in a Specific Album, see 'Album Shortcodes' page for shortcodes complete with your Album IDs. Only one album per page.(PRO VERSION ONLY)
[cws_gpp_images_by_albumid_gp id=5218507736478682657 theme=grid show_title=0 show_details=0]




There are 2 main shortcodes:

* [cws_gpp_albums_gphotos access=own]
* [cws_gpp_images_in_album_gphotos]


Each shortcode will work as is by using the default values saved on the plugin settings page.

The defaults can be overridden by placing attribites on the shortcode below are some examples.


*[cws_gpp_albums_gphotos access=own]*

Attributes descriptions and examples

theme='grid|list|carousel'
results_page='album-results|page-slug-here'
show_title='1|0'
show_details='1|0'
thumb_size='150'
num_results='9'
hide_albums='Auto Backup,name of album here'

For example
[cws_gpp_albums_gphotos access=own theme='grid' results_page='results-list' show_title=1 show_details=1 thumb_size='50' num_results='9']



*[cws_gpp_images_in_album_gphotos]*

theme='grid|list|carousel'
album_title='1|0'
show_title='1|0'
show_details='1|0'
thumb_size='150'
num_results='9'
crop=1

For example
[cws_gpp_images_in_album_gphotos theme='grid' album_title=1 show_title=1  show_details=1 crop=1]


== Credits ==
* Google Photos Gallery - https://wordpress.org/plugins/google-picasa-albums-viewer/

== Changelog ==

= 1.0.0 =
* Forked from project https://wordpress.org/plugins/google-picasa-albums-viewer/

= 1.1.0 =
* Removed debug messages

= 1.1.0 =
* Fixed XSS on wp single album page
* Disabled debug flag for any user

= 1.1.2 =
* Updated plugin name

= 1.1.3 =
* Fixed problems with compatibility with php 8.0
